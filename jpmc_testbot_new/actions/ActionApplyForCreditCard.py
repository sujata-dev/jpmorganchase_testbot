from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet, AllSlotsReset
from rasa_core.events import Restarted

from SlotFill import SlotFill


class ActionApplyForCreditCard(Action):
    def name(self):
        return 'action_apply_for_creditcard'

    def run(self, dispatcher, tracker, domain):

        if(tracker.get_slot("creditcard_name")):
            slot_list = ["name", "address", "zip_code", "ssn", "mail_id"]

            obj = SlotFill(slot_list)
            obj.slot_checker(dispatcher, tracker, domain)

            if tracker.get_slot("matches") == True:
                dispatcher.utter_template("utter_applied_for_creditcard",
                    p=tracker.get_slot("creditcard_name"),
                    q=tracker.get_slot("name"),
                    r=tracker.get_slot("address"),
                    s=tracker.get_slot("zip_code"),
                    t=tracker.get_slot("ssn"),
                    u=tracker.get_slot("mail_id"))

                return [AllSlotsReset()]

        else:
            if(tracker.get_slot("credit_card_purpose") or tracker.get_slot("travel_type")):
                tracker.update(SlotSet("matches", True))
            else:
                slot_list = ["credit_card_type", "credit_card_purpose"]

                obj = SlotFill(slot_list)
                obj.slot_checker(dispatcher, tracker, domain)

            if tracker.get_slot("matches") == True:
                if tracker.get_slot("credit_card_purpose") or tracker.get_slot("travel_type"):
                    flag = 1;
                    if tracker.get_slot("travel_type") or tracker.get_slot("credit_card_purpose") == "travel":
                        if tracker.get_slot("travel_type"):
                            if tracker.get_slot("travel_type") == "airline":
                                dispatcher.utter_template("utter_individual_travel_airline")
                                dispatcher.utter_template("utter_which_one")
                            elif tracker.get_slot("travel_type") == "hotel":
                                dispatcher.utter_template("utter_individual_travel_hotel")
                                dispatcher.utter_template("utter_which_one")
                            else:
                                dispatcher.utter_template("utter_credit_card_purpose_notfound")

                        else:
                            dispatcher.utter_template("utter_travel_type")
                            flag = 0;

                    elif tracker.get_slot("credit_card_purpose") == "featured":
                        dispatcher.utter_template("utter_individual_featured")
                        dispatcher.utter_template("utter_which_one")
                    elif tracker.get_slot("credit_card_purpose") == "cash back" or tracker.get_slot("credit_card_purpose") == "cashback":
                        dispatcher.utter_template("utter_individual_cashback")
                        dispatcher.utter_template("utter_which_one")
                    elif tracker.get_slot("credit_card_purpose") == "balance transfer":
                        dispatcher.utter_template("utter_individual_balancetransfer")
                        dispatcher.utter_template("utter_which_one")
                    elif tracker.get_slot("credit_card_purpose") == "small business" or tracker.get_slot("credit_card_purpose") == "business":
                        dispatcher.utter_template("utter_business_smallbusiness")
                        dispatcher.utter_template("utter_which_one")
                    else:
                        dispatcher.utter_template("utter_credit_card_purpose_notfound")

                if flag == 1:
                    return [AllSlotsReset()]

        return []
