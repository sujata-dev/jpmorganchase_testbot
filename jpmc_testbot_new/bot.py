#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import sys
from rasa_core.agent import Agent
from rasa_core.actions import Action
from rasa_core.channels.console import ConsoleInputChannel
from rasa_core.interpreter import RasaNLUInterpreter
from rasa_core.policies.keras_policy import KerasPolicy
from rasa_core.policies.memoization import MemoizationPolicy
from rasa_core.tracker_store import InMemoryTrackerStore
from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer
from VRAPolicy import VRAPolicy
from rasa_core.domain import TemplateDomain


def train_nlu():
    print("Loading training data from data/training_data.json")
    training_data = load_data('data/training_data.json')

    trainer = Trainer(RasaNLUConfig("config.json"))
    trainer.train(training_data)
    print("Training in progress...")
    model_directory = trainer.persist('projects/', fixed_model_name = "model")
    print("Training complete.")
    print("NLU classifier is ready for action.")
    return model_directory

def train_dialogue(domain_file="domain.yml",
                   model_path="models/dialogue",
                   training_data_file="data/stories.md"):

    print("Loading stories from data/stories.md")
    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(), VRAPolicy()])

    agent.train(
            training_data_file,
            augmentation_factor=50,
            max_history=2,
            epochs=500,
            batch_size=10,
            validation_split=0.2
    )

    agent.persist(model_path)
    print("Now I have learnt to talk, try me out!")
    return agent


def run(serve_forever=True):
    default_domain = TemplateDomain.load("./domain.yml")
    NLU_Interpreter = RasaNLUInterpreter("projects/default/model")

    agent = Agent.load("models/dialogue",
              interpreter=NLU_Interpreter)

    if serve_forever:
        agent.handle_channel(ConsoleInputChannel())
    return agent


def train_online(input_channel=ConsoleInputChannel(),
                        interpreter=RasaNLUInterpreter("projects/default/model"),
                          domain_file="domain.yml",
                          training_data_file='data/stories.md'):

    agent = Agent(domain_file,
                  policies=[MemoizationPolicy(), KerasPolicy()],
                  interpreter=interpreter)

    agent.train_online(training_data_file,
                       input_channel=input_channel,
                       max_history=2,
                       batch_size=50,
                       epochs=200,
                       max_training_samples=300)

    return agent


if __name__ == '__main__':
    while (1):
        print("\nSelect any one:\n1) Chat with me\n2) Trigger training for NLU engine\n3) Trigger training for conversation policy \n4) Train me to converse \n5) Exit")
        task = raw_input()
        if task == "1":
            print("Please wait...")
            run()
        elif task == "2":
            train_nlu()
        elif task == "3":
            print("Train Policy...")
            train_dialogue()
        elif task == "4":
            print("Train Online...")
            train_online()
        elif task == "5":
            print("Exiting.")
            sys.exit(1)
        else:
            print("Enter a valid option")


