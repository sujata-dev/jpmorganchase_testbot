## Generated Story 5441538340371847535
* _greet
    - utter_greet
## Generated Story 6714411449514096626
* apply_for_credit_card
    - slot{"matches": false}
    - slot{"matches": false}
    - action_apply_for_creditcard
* apply_for_credit_card{"credit_card_type": "individual"}
    - slot{"credit_card_type": "individual"}
    - slot{"matches": false}
    - slot{"matches": true}
    - slot{"matches": false}
    - action_apply_for_creditcard
* apply_for_credit_card{"credit_card_purpose": "travel"}
    - slot{"credit_card_purpose": "travel"}
    - slot{"matches": true}
    - action_apply_for_creditcard
* apply_for_credit_card{"travel_type": "airline"}
    - slot{"travel_type": "airline"}
    - slot{"matches": true}
    - action_apply_for_creditcard
    - reset_slots
* apply_for_credit_card{"ORG": "Chase Sapphire Reserve", "creditcard_name": "chase sapphire reserve"}
    - slot{"creditcard_name": "chase sapphire reserve"}
    - slot{"matches": false}
    - slot{"matches": false}
    - action_apply_for_creditcard
* apply_for_credit_card{"name": "kanad"}
    - slot{"name": "kanad"}
    - slot{"matches": false}
    - slot{"matches": true}
    - slot{"matches": false}
    - action_apply_for_creditcard
* apply_for_credit_card{"mail_id": "kanad@gmail.com"}
    - slot{"mail_id": "kanad@gmail.com"}
    - slot{"matches": false}
    - slot{"matches": true}
    - slot{"matches": true}
    - action_apply_for_creditcard
    - reset_slots
* greet
    - utter_greet
    - export
