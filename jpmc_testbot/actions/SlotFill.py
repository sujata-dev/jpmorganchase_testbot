from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet
from rasa_core.events import Restarted

required_key = None
class SlotFill:
    slot_list = []

    def __init__(self, slot_list):
        self.slot_list = slot_list

    def slot_checker(self, dispatcher, tracker, domain):
        tracker.update(SlotSet("matches", False))

        for recent_key in self.slot_list:
            if next(tracker.get_latest_entity_values(recent_key), None):
                break
            else:
                recent_key = None

        values = []
        for slot in self.slot_list:
            values.append(tracker.get_slot(slot))
        values = [x for x in values if x is not None]
        for slot_key in self.slot_list:
            global required_key
            if recent_key and recent_key != required_key:
                # dispatcher.utter_template("utter_confirm", x=recent_key,
                #                           y=tracker.get_slot(recent_key))
                recent_key = None
            if values:
                tracker.update(SlotSet("matches", True))
                break

            else:
                required_key=slot_key
                tracker.update(SlotSet("matches", False))
                text = "utter_" + slot_key
                if text in dispatcher.domain.templates.keys():
                    dispatcher.utter_template(text)

                else:
                    dispatcher.utter_template("utter_default", x=slot_key)
                break
