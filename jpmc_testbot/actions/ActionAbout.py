from rasa_core.actions import Action
from rasa_core.events import SlotSet, TopicSet, AllSlotsReset
from rasa_core.events import Restarted

from SlotFill import SlotFill


class ActionAbout(Action):
    def name(self):
        return 'action_about'

    def run(self, dispatcher, tracker, domain):
        slot_list = ["repair_eye", "oil_control"]

        obj = SlotFill(slot_list)
        obj.slot_checker(dispatcher, tracker, domain)

        if tracker.get_slot("matches") == True:
            for recent_key in slot_list:
                if next(tracker.get_latest_entity_values(recent_key), None):
                    tracker.update(SlotSet("product_type", recent_key))
                    break
                else:
                    recent_key = None

            if tracker.get_slot("product_type"):
                if tracker.get_slot("product_type") == "repair_eye":
                    dispatcher.utter_template("utter_about_eye")
                elif tracker.get_slot("product_type") == "oil_control":
                    dispatcher.utter_template("utter_about_oil")
        return []
